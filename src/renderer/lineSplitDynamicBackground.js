import { SQUARE_SIZE } from '$/constants'
import SQUARE_TYPE from '$/util/SQUARE_TYPE'

export default function (state, canvas, context) {
  let dominantElementColor

  if (state.share.fire > state.share.tree && state.share.fire > state.share.water) {
    dominantElementColor = SQUARE_TYPE.FIRE.color.static
  } else if (state.share.tree > state.share.fire && state.share.tree > state.share.water) {
    dominantElementColor = SQUARE_TYPE.TREE.color.static
  } else {
    dominantElementColor = SQUARE_TYPE.WATER.color.static
  }

  context.fillStyle = dominantElementColor
  context.fillRect(0, 0, canvas.width, canvas.height)

  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]

    let previousColor = null
    let previousCount = 0

    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      const currentColor = column.current.color.static

      if ((previousColor !== currentColor && previousColor !== null) ||
        x + 1 === line.length) {
        if (previousColor !== dominantElementColor) {
          context.fillStyle = previousColor
          context.fillRect((x - previousCount) * SQUARE_SIZE, y * SQUARE_SIZE,
            SQUARE_SIZE * previousCount, SQUARE_SIZE)
        }

        previousColor = currentColor
        previousCount = 0
      }

      if (previousColor === null) {
        previousColor = currentColor
      }

      if (previousColor === currentColor) {
        ++previousCount
      }

      if (x + 1 === line.length) {
        context.fillStyle = currentColor
        context.fillRect(x * SQUARE_SIZE, y * SQUARE_SIZE, SQUARE_SIZE,
          SQUARE_SIZE)
      }
    }
  }
}
