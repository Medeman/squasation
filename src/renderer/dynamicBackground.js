import { SQUARE_SIZE } from '$/constants'
import SQUARE_TYPE from '$/util/SQUARE_TYPE'

export default function (state, canvas, context) {
  let dominantElement

  if (state.share.fire > state.share.tree && state.share.fire > state.share.water) {
    dominantElement = SQUARE_TYPE.FIRE
  } else if (state.share.tree > state.share.fire && state.share.tree > state.share.water) {
    dominantElement = SQUARE_TYPE.TREE
  } else {
    dominantElement = SQUARE_TYPE.WATER
  }

  context.fillStyle = dominantElement.color.static
  context.fillRect(0, 0, canvas.width, canvas.height)

  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]
    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      if (column.current === dominantElement) {
        continue
      }

      context.fillStyle = column.current.color.static
      context.fillRect(x * SQUARE_SIZE, y * SQUARE_SIZE, SQUARE_SIZE,
        SQUARE_SIZE)
    }
  }
}
