import { SQUARE_SIZE } from '$/constants'

export default function (state, canvas, context) {
  context.fillStyle = 'rgb(0, 0, 0)'
  context.fillRect(0, 0, canvas.width, canvas.height)

  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]
    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      context.fillStyle = column.current.color.dynamic
      context.fillRect(x * SQUARE_SIZE, y * SQUARE_SIZE, SQUARE_SIZE,
        SQUARE_SIZE)
    }
  }
}
