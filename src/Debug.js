/**
 * Debug overlay class.
 */
export default class Debug {
  constructor (selector) {
    this.container = document.querySelector(selector)

    if (this.container === null) {
      throw new Error('Debug container element does not exist!')
    }

    this.lines = []
  }

  /**
   * Add a line of text to the debug output.
   */
  add (text) {
    this.lines.push(...text.split('\n'))
  }

  /**
   * Print all added lines. Also resets the line array.
   */
  print () {
    this.container.innerHTML = this.lines
      .join('<br>')
      .replace(/\s/g, '&nbsp;')

    this.lines = []
  }
}
