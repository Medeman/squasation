/*

Possible performance improvements:
  Use a flat array instead of a nested one (~7%).
  See https://jsperf.com/nested-array-loop-vs-flat-array-loop

  Render in an OffscreenCanvas.
  See https://developers.google.com/web/updates/2018/08/offscreen-canvas
  Maybe the canvas could be split into multiple canvas (i.e. 4) and multiple
  workers could be used to render a part of the image each.

*/

import Debug from '$/Debug'
import random from '$/util/random'
import initState from '$/util/initState'
import { SQUARE_SIZE } from '$/constants'
import ResizeHandler from '$/util/ResizeHandler'
import config from '$/util/config'
import SQUARE_TYPE from '$/util/SQUARE_TYPE'

// Renderer
import dynamicBackground from '$/renderer/dynamicBackground'
import basic from '$/renderer/basic'
import grain from '$/renderer/grain'
import lineSplit from '$/renderer/lineSplit'
import lineSplitDynamicBackground from '$/renderer/lineSplitDynamicBackground'

const availableRenderers = {
  lineSplitDynamicBackground,
  lineSplit,
  dynamicBackground,
  basic,
  grain
}

let activeRenderer = lineSplitDynamicBackground

const canvas = document.querySelector('#canvas')
const context = canvas.getContext('2d')

const FRAMES_PER_EVOLUTION = 1

const typeOfSquare = square => {
  if (square === SQUARE_TYPE.FIRE) {
    return 'fire'
  }

  if (square === SQUARE_TYPE.TREE) {
    return 'tree'
  }

  if (square === SQUARE_TYPE.WATER) {
    return 'water'
  }
}

const state = initState()

const debug = new Debug('#debug')

const generateSquares = () => {
  state.squares = []
  state.evolution = 0

  for (let y = 0; y < state.size.y; ++y) {
    const line = []

    state.squares.push(line)

    for (let x = 0; x < state.size.x; ++x) {
      const r = random(1, 3)

      if (r === 1) {
        line.push({
          current: SQUARE_TYPE.FIRE
        })
      }

      if (r === 2) {
        line.push({
          current: SQUARE_TYPE.TREE
        })
      }

      if (r === 3) {
        line.push({
          current: SQUARE_TYPE.WATER
        })
      }
    }
  }

  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]

    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      column.surroundings = []

      // Top
      if (y > 0) {
        column.surroundings.push(state.squares[y - 1][x])
      }

      // Top-right
      if (y > 0 && x + 1 < state.size.x) {
        column.surroundings.push(state.squares[y - 1][x + 1])
      }

      // Right
      if (x + 1 < state.size.x) {
        column.surroundings.push(state.squares[y][x + 1])
      }

      // Bottom-right
      if (y + 1 < state.size.y && x + 1 < state.size.x) {
        column.surroundings.push(state.squares[y + 1][x + 1])
      }

      // Bottom
      if (y + 1 < state.size.y) {
        column.surroundings.push(state.squares[y + 1][x])
      }

      // Bottom-left
      if (y + 1 < state.size.y && x > 0) {
        column.surroundings.push(state.squares[y + 1][x - 1])
      }

      // Left
      if (x > 0) {
        column.surroundings.push(state.squares[y][x - 1])
      }

      // Top-left
      if (y > 0 && x > 0) {
        column.surroundings.push(state.squares[y - 1][x - 1])
      }
    }
  }
}

const simulate = () => {
  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]

    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      const surroundings = {
        fire: 0,
        tree: 0,
        water: 0
      }

      for (let s of column.surroundings) {
        ++surroundings[typeOfSquare(s.current)]
      }

      const result = column.current.calculate(surroundings)

      if (result) {
        column.next = result
      }
    }
  }

  state.share = {
    fire: 0,
    tree: 0,
    water: 0
  }

  for (let y = 0; y < state.squares.length; ++y) {
    const line = state.squares[y]

    for (let x = 0; x < line.length; ++x) {
      const column = line[x]

      if (column.next) {
        column.current = column.next
        column.next = null
      }

      ++state.share[typeOfSquare(column.current)]
    }
  }
}

const loop = () => {
  requestAnimationFrame(loop)
  ++state.frame

  if (state.frame % FRAMES_PER_EVOLUTION > 0) {
    return
  }

  simulate()

  ++state.evolution

  activeRenderer(state, canvas, context)

  debug.add(`Frame: ${state.frame}`)
  debug.add(`Evolution: ${state.evolution}`)
  debug.add('')
  debug.add('Probabilities:')
  debug.add(`  Fire -> Tree ${config.probabilities.fire.tree}%`)
  debug.add(`          Water ${config.probabilities.fire.water}%`)
  debug.add(`  Tree -> Fire ${config.probabilities.tree.fire}%`)
  debug.add(`          Water ${config.probabilities.tree.water}%`)
  debug.add(`  Water -> Fire ${config.probabilities.water.fire}%`)
  debug.add(`           Tree ${config.probabilities.water.tree}%`)
  // debug.add(`Transforms:
  //   Fire ${state.transforms.fire} |
  //   Tree ${state.transforms.tree} |
  //   Water ${state.transforms.water}`)
  debug.add('')
  debug.add('Share:')
  debug.add(`  Fire ${Math.round(state.share.fire / (state.size.x * state.size.y) * 100)}%`)
  debug.add(`  Tree ${Math.round(state.share.tree / (state.size.x * state.size.y) * 100)}%`)
  debug.add(`  Water ${Math.round(state.share.water / (state.size.x * state.size.y) * 100)}%`)
  // debug.add('')
  // debug.add('RNG buffer remaining:')
  // debug.add(`  ${RNG_BUFFER_SIZE - randomIndex}`)
  // debug.add('')
  // debug.add(`Dominant element: ${typeOfSquare(dominantElement)}`)
  debug.print()
}

const init = () => {
  const resizeHandler = new ResizeHandler(canvas, state.size, () => {
    generateSquares()
  })

  resizeHandler.register(window)
  resizeHandler.resize()

  const rendererSelect = document.querySelector('#renderer')

  for (let i in availableRenderers) {
    const option = document.createElement('option')
    option.value = i
    option.innerText = i

    rendererSelect.appendChild(option)
  }

  rendererSelect.addEventListener('change', function () {
    activeRenderer = availableRenderers[this.value]
  })

  loop()
}

init()
