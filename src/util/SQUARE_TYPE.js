import DynamicColor from '$/util/DynamicColor'
import random from '$/util/random'
import config from '$/util/config'

const happens = (from, to) => {
  return random(0, 100) < config.probabilities[from][to]
}

const SQUARE_TYPE = {
  FIRE: {
    color: new DynamicColor(39, 100, 50),
    calculate (surroundings) {
      if (surroundings.tree === 8 && happens('fire', 'tree')) {
        return SQUARE_TYPE.TREE
      }

      if (surroundings.water >= 2 && happens('fire', 'water')) {
        return SQUARE_TYPE.WATER
      }
    }
  },
  TREE: {
    color: new DynamicColor(120, 100, 25),
    calculate (surroundings) {
      if (surroundings.water === 8 && happens('tree', 'water')) {
        return SQUARE_TYPE.WATER
      }

      if (surroundings.fire >= 2 && happens('tree', 'fire')) {
        return SQUARE_TYPE.FIRE
      }
    }
  },
  WATER: {
    color: new DynamicColor(240, 100, 50),
    calculate (surroundings) {
      if (surroundings.fire === 8 && happens('water', 'fire')) {
        return SQUARE_TYPE.FIRE
      }

      if (surroundings.tree >= 2 && happens('water', 'tree')) {
        return SQUARE_TYPE.TREE
      }
    }
  }
}

export default SQUARE_TYPE
