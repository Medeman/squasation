import random from '$/util/random'

export default class DynamicColor {
  constructor (hue, saturation, lightness) {
    this.hue = hue
    this.saturation = saturation
    this.lightness = lightness

    this.static = `hsl(${this.hue}, ${this.saturation}%, ${this.lightness}%)`
  }

  get dynamic () {
    return `hsl(${this.hue}, ${this.saturation}%, ${this.lightness + random(-10, 10)}%`
  }
}
