export default function initState () {
  return {
    frame: 0,
    evolution: 0,
    squares: [],
    share: {
      fire: 0,
      tree: 0,
      water: 0
    },
    size: {
      x: 0,
      y: 0
    }
  }
}
