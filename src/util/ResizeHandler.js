import { SQUARE_SIZE } from '$/constants'

export default class ResizeHandler {
  constructor (canvas, size, callback = () => {}) {
    this.canvas = canvas
    this.size = size
    this.callback = callback
  }

  register (target) {
    target.addEventListener('resize', () => {
      this.resize()
    })
  }

  resize () {
    this.canvas.width = window.innerWidth
    this.canvas.height = window.innerHeight

    this.size.x = Math.ceil(this.canvas.width / SQUARE_SIZE)
    this.size.y = Math.ceil(this.canvas.height / SQUARE_SIZE)

    this.callback()
  }
}
