const RNG_BUFFER_SIZE = 10000
const MAX_RANGE = 256

let randomBuffer = null
let randomIndex = 0

export default function random (min, max) {
  if (randomBuffer === null || randomIndex >= RNG_BUFFER_SIZE) {
    randomBuffer = new Uint8Array(RNG_BUFFER_SIZE)
    crypto.getRandomValues(randomBuffer)
    randomIndex = 0
  }

  const randomByte = randomBuffer[randomIndex]
  ++randomIndex

  const range = max - min + 1

  if (randomByte >= Math.floor(MAX_RANGE / range) * range) {
    return random(min, max)
  }

  return min + (randomByte % range)
}
