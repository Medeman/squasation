import random from '$/util/random'

export default {
  probabilities: {
    fire: {
      tree: random(0, 100),
      water: random(0, 100)
    },
    tree: {
      fire: random(0, 100),
      water: random(0, 100)
    },
    water: {
      fire: random(0, 100),
      tree: random(0, 100)
    }
  }
}
